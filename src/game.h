#ifndef GAME_H
#define GAME_H

typedef enum {I, Z, S, J, L, O, T} piece_type;
typedef enum {_0_, CCW, _180_, CW} rotation;
typedef enum {LEFT = -1, RIGHT = 1} movement;

struct piece_state {
	piece_type type;
	rotation rot;
	int x;
	int y;
};

struct rng_state {
	uint hash;
	int hist[4];
};

struct coords {
	int y;
	int x;
};

static const struct coords shape[7][4][4] =
{
	{	/* I piece */
		{{1, 0}, {1, 1}, {1, 2}, {1, 3}},
		{{3, 2}, {2, 2}, {1, 2}, {0, 2}},
		{{1, 0}, {1, 1}, {1, 2}, {1, 3}},
		{{3, 2}, {2, 2}, {1, 2}, {0, 2}}
	},
	{	/* Z piece */
		{{2, 1}, {2, 2}, {1, 0}, {1, 1}},
		{{2, 1}, {1, 1}, {1, 2}, {0, 2}},
		{{2, 1}, {2, 2}, {1, 0}, {1, 1}},
		{{2, 1}, {1, 1}, {1, 2}, {0, 2}}
	},
	{	/* S piece */
		{{2, 0}, {2, 1}, {1, 1}, {1, 2}},
		{{2, 1}, {1, 0}, {1, 1}, {0, 0}},
		{{2, 0}, {2, 1}, {1, 1}, {1, 2}},
		{{2, 1}, {1, 0}, {1, 1}, {0, 0}}
	},
	{	/* J piece */
		{{2, 2}, {1, 0}, {1, 1}, {1, 2}},
		{{2, 1}, {1, 1}, {0, 1}, {0, 2}},
		{{2, 0}, {2, 1}, {2, 2}, {1, 0}},
		{{2, 0}, {2, 1}, {1, 1}, {0, 1}}
	},
	{	/* L piece */
		{{2, 0}, {1, 0}, {1, 1}, {1, 2}},
		{{2, 1}, {2, 2}, {1, 1}, {0, 1}},
		{{2, 0}, {2, 1}, {2, 2}, {1, 2}},
		{{2, 1}, {1, 1}, {0, 0}, {0, 1}}
	},
	{	/* O piece */
		{{2, 1}, {2, 2}, {1, 1}, {1, 2}},
		{{2, 1}, {2, 2}, {1, 1}, {1, 2}},
		{{2, 1}, {2, 2}, {1, 1}, {1, 2}},
		{{2, 1}, {2, 2}, {1, 1}, {1, 2}}
	},
	{	/* T piece */
		{{2, 1}, {1, 0}, {1, 1}, {1, 2}},
		{{2, 1}, {1, 1}, {1, 2}, {0, 1}},
		{{2, 0}, {2, 1}, {2, 2}, {1, 1}},
		{{2, 1}, {1, 0}, {1, 1}, {0, 1}}
	}	
};


int hash (uint *hash);
int rng_tap (struct rng_state *rng);
void lock_piece (int stack[30][10], struct piece_state *piece, int *height);
int spawn_check (int stack[30][10], struct piece_state *piece);
void add_piece (int stack[30][10], struct piece_state *piece);
void remove_piece (int stack[30][10], struct piece_state *piece);
void draw_stack (int stack[30][10],
	struct piece_state *piece,
	struct piece_state *next_piece,
	int level,
	struct timespec *elapsed_time);
void fall_20g (int stack[30][10], struct piece_state *piece, int *height);
int move (int stack[30][10], struct piece_state *piece, int direction);
void rotate (int stack[30][10], struct piece_state *piece, rotation direction);
int line_clear (int stack[30][10], struct piece_state *piece, int *height);
int input_sequence (char input[], int stack[30][10], struct piece_state *piece, int *height);

#endif /* GAME_H */