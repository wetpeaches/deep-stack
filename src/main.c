#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "game.h"

int main(int argc, char *argv[]) {

	int stack[30][10];
	for (int i = 0; i < 30; i++) {
		for (int j = 0; j < 10; j++) {
			stack[i][j] = 0;
		}
	}

	struct rng_state rng = {0, Z, Z, S, S};

	struct timespec start_time, current_time, elapsed_time;
	clock_gettime(CLOCK_MONOTONIC, &start_time);
	elapsed_time.tv_sec = 0;
	elapsed_time.tv_nsec = 0;
	rng.hash = start_time.tv_sec * start_time.tv_nsec;

	struct piece_state piece = {hash(&rng.hash), 0, 3, 24};
	while (piece.type == S || piece.type == Z || piece.type == O)
		piece.type = hash(&rng.hash);
	rng.hist[0] = piece.type;
	struct piece_state next_piece = {rng_tap(&rng), 0, 3, 24};

	char input[20];
	int height = 0;
	int level = 0;
	int game_over = 0;

	draw_stack(stack, &piece, &piece, level, &elapsed_time);
	piece.y = 20;
	while(!game_over) {

		for (int i = 0; i < 20; i++) {
			input[i] = getchar();
			if (input[i] == '\n')
				break;
		}

		clock_gettime(CLOCK_MONOTONIC, &current_time);
		elapsed_time.tv_sec = current_time.tv_sec - start_time.tv_sec;
		elapsed_time.tv_nsec = current_time.tv_nsec - start_time.tv_nsec;
		if (start_time.tv_nsec > current_time.tv_nsec) {
			elapsed_time.tv_sec -= 1;
			elapsed_time.tv_nsec += 1000000000;
		}

		game_over = input_sequence(input, stack, &piece, &height);
		lock_piece(stack, &piece, &height);

		if (!game_over)
			level += line_clear(stack, &piece, &height);
		if (level % 100 != 99)
			level += 1;
		if (level >= 999) {
			level = 999;
			game_over = 1;
		}

		draw_stack(stack, &next_piece, &next_piece, level, &elapsed_time);
		piece = next_piece;
		piece.y = 20;
		next_piece.type = rng_tap(&rng);

	}

	printf("        GAME OVER!\n\n");

	return 0;
}