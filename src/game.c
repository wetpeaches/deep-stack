#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "game.h"

int hash (uint *hash) {
	*hash *= 1103515245;
	*hash += 12345;
	return ((*hash >> 10) & 0x7FFF) % 7;
}

int rng_tap (struct rng_state *rng) {
	int piece;
	
	for (int roll = 0; roll < 5; roll++) {
		piece = hash(&rng->hash);
		if (piece == rng->hist[0] ||
		    piece == rng->hist[1] ||
		    piece == rng->hist[2] ||
		    piece == rng->hist[3]) 
			piece = hash(&rng->hash);
			continue;
		break;
	}
	
	rng->hist[3] = rng->hist[2];
	rng->hist[2] = rng->hist[1];
	rng->hist[1] = rng->hist[0];
	rng->hist[0] = piece;

	return piece;
}

void lock_piece(int stack[30][10], struct piece_state *piece, int *height) {

	int i, j;

	for (int square = 0; square < 4; square++) {
		i = piece->y - shape[piece->type][piece->rot][square].y;
		j = piece->x + shape[piece->type][piece->rot][square].x;
		if (i > *height)
			*height = i;
		stack[i][j] = 1;
	}
}

int spawn_check(int stack[30][10], struct piece_state *piece) {

	int i, j;
	int dead = 0;

	for (int square = 0; square < 4; square++) {
		i = piece->y - shape[piece->type][piece->rot][square].y;
		j = piece->x + shape[piece->type][piece->rot][square].x;
		dead += stack[i][j];
	}
	return dead;
}

void add_piece(int stack[30][10], struct piece_state *piece) {

	int i, j;

	for (int square = 0; square < 4; square++) {
		i = piece->y - shape[piece->type][piece->rot][square].y;
		j = piece->x + shape[piece->type][piece->rot][square].x;
		stack[i][j] = 1;
	}
}

void remove_piece(int stack[30][10], struct piece_state *piece) {

	int i, j;

	for (int square = 0; square < 4; square++) {
		i = piece->y - shape[piece->type][piece->rot][square].y;
		j = piece->x + shape[piece->type][piece->rot][square].x;
		stack[i][j] = 0;
	}
}

void draw_stack(int stack[30][10],
	struct piece_state *piece,
	struct piece_state *next_piece,
	int level,
	struct timespec *elapsed_time) {

	add_piece(stack, piece);
	add_piece(stack, next_piece);

	for(int i = 0; i <= 25; i++) {
		if (25 - i <= 19)
			printf("  ║");
		else
			printf("   ");
		for (int j = 0; j < 10; j++) {
			switch(stack[25 - i][j])
			{
				case 0:
					printf("  ");
					break;
				case 1:
					printf("██");
					break;
				default:
					printf("\ninvalid value %d detected\n", stack[20-i][j]);
					break;
			}
		}
		if (25 - i <= 19)
			printf("║");
		printf("\n");
	}
	printf("  ╚════════════════════╝    lvl%03d\n", level);

	uint minutes = (uint) elapsed_time->tv_sec / 60;
	uint seconds = (uint) elapsed_time->tv_sec % 60;
	uint hours = (uint) elapsed_time->tv_nsec / 10000000;
	printf("         %02d:%02d:%02d         \n\n", minutes, seconds, hours);

	remove_piece(stack, piece);
	remove_piece(stack, next_piece);

	return;
}

void fall_20g (int stack[30][10], struct piece_state *piece, int *height) {

	int i, j;

	if (piece->y - shape[piece->type][piece->rot][0].y > *height)
		piece->y = *height + shape[piece->type][piece->rot][0].y + 1;

	while (1) {
		for (int square = 0; square < 4; square++) {
			i = piece->y - shape[piece->type][piece->rot][square].y;
			j = piece->x + shape[piece->type][piece->rot][square].x;
			if (i == 0 || stack[i - 1][j] == 1)
				return;
		}
		piece->y -= 1;
	}
}

int move (int stack[30][10], struct piece_state *piece, movement direction) {

	int i, j;

	for (int square = 0; square < 4; square++) {
		i = piece->y - shape[piece->type][piece->rot][square].y;
		j = piece->x + shape[piece->type][piece->rot][square].x + direction;
		if (j < 0 || j > 9 || stack[i][j] == 1)
			return 0;
	}
	
	piece->x += direction;	
	return 1;

}

void rotate (int stack[30][10], struct piece_state *piece, rotation direction) {

	const int rot = (piece->rot + direction) % 4;
	int i, j;

	// basic rotation
	for (int square = 0; square < 4; square++) {
		i = piece->y - shape[piece->type][rot][square].y;
		j = piece->x + shape[piece->type][rot][square].x;
		if (i < 0 || j < 0 || j > 9 || stack[i][j] == 1)
			break;
		if (square == 3) {
			piece->rot = rot;
			return;			
		}
	}

	// exceptions -- no wallkick allowed
	if (piece->type == I)
		return;

	if (piece->type == L && rot == CCW) {
		i = piece->y - 0;
		j = piece->x + 1;
		if (stack[i][j] == 1 || stack[i-1][j] == 1 || stack[i-2][j] == 1)
			return;
	}

	if (piece->type == L && rot == CW) {
		i = piece->y - 0;
		j = piece->x + 0;
		if (!(stack[i][j] == 1))
			return;
	}

	if (piece->type == J && rot == CW) {
		i = piece->y - 0;
		j = piece->x + 1;
		if (stack[i][j] == 1 || stack[i-1][j] == 1 || stack[i-2][j] == 1)
			return;
	}

	if (piece->type == J && rot == CCW) {
		i = piece->y - 0;
		j = piece->x + 2;
		if (!(stack[i][j] == 1))
			return;
	}

	if (piece->type == T && (rot == CCW || rot == CW)) {
		i = piece->y - 0;
		j = piece->x + 1;
		if (stack[i][j] == 1)
			return;
	}

	// kicks
	for (int square = 0; square < 4; square++) {
		i = piece->y - shape[piece->type][rot][square].y;
		j = piece->x + shape[piece->type][rot][square].x + 1;
		if (j < 0 || j > 9 || stack[i][j] == 1)
			break;
		if (square == 3) {
			piece->x += 1;
			piece->rot = rot;
			return;			
		}
	}

	for (int square = 0; square < 4; square++) {
		i = piece->y - shape[piece->type][rot][square].y;
		j = piece->x + shape[piece->type][rot][square].x - 1;
		if (j < 0 || j > 9 || stack[i][j] == 1)
			break;
		if (square == 3) {
			piece->x -= 1;
			piece->rot = rot;
			return;			
		}
	}
}

int line_clear(int stack[30][10], struct piece_state *piece, int *height) {

	int lines = 0;
	const int i_min = piece->y - shape[piece->type][piece->rot][0].y;
	const int i_max = piece->y - shape[piece->type][piece->rot][3].y;
	int i, j;

	for (i = i_min; i <= *height + lines; i++) {

		for (j = 0; j < 10; j++){
			if (stack[i][j] == 0)
				break;
		}

		if (j == 10) 
			lines += 1;
		else if (lines > 0) {
			for (int j2 = 0; j2 < 10; j2++){
				stack[i - lines][j2] = stack[i][j2];
			}
		}
	}

	*height -= lines;
	return lines;
}

int input_sequence(char input[], int stack[30][10], struct piece_state *piece, int *height) {

	int game_over = 0;
	int input_i = 0;
	int direction = 0;

	switch(input[0])
	{
		case '?':
			game_over = spawn_check(stack, piece);
			add_piece(stack, piece);
			return game_over;
		case 'w':
			input_i++;
		case 'a':
		case 'A':
		case 's':
		case 'd':
		case 'D':
		case '\n':
			game_over = spawn_check(stack, piece);
			break;
		case 'j':
		case 'l':
		case 'J':
		case 'L':
			piece->rot = CCW;
			game_over = spawn_check(stack, piece);
			if (game_over) {
				piece->rot = _0_;
				game_over = spawn_check(stack, piece);
			}
			input_i++;
			break;
		case 'k':
		case 'K':
			piece->rot = CW;
			game_over = spawn_check(stack, piece);
			if (game_over) {
				piece->rot = _0_;
				game_over = spawn_check(stack, piece);
			}
			input_i++;
			break;
		default:
			break;
	}

	if (game_over)
		return game_over;
	fall_20g(stack, piece, height);

	while (input[input_i] != '\n') {
		switch(input[input_i])
		{
			case 'w':
				break;

			case 'A':
				while(move(stack, piece, LEFT))
					fall_20g(stack, piece, height);
				direction = LEFT;
				break;

			case 'a':
				move(stack, piece, LEFT);
				fall_20g(stack, piece, height);
				break;

			case 's':
				return game_over;
				break;

			case 'D':
				while(move(stack, piece, RIGHT))
					fall_20g(stack, piece, height);
				direction = RIGHT;
				break;

			case 'd':
				move(stack, piece, RIGHT);
				fall_20g(stack, piece, height);
				break;

			case 'J':
			case 'L':
				rotate(stack, piece, CCW);
				if (direction != 0) {
					move(stack, piece, direction);
					fall_20g(stack, piece, height);
					while(move(stack, piece, direction))
						fall_20g(stack, piece, height);
				} else {
					fall_20g(stack, piece, height);
				}
				break;

			case 'j':
			case 'l':
				rotate(stack, piece, CCW);
				fall_20g(stack, piece, height);
				break;

			case 'K':
				rotate(stack, piece, CW);
				if (direction != 0) {
					move(stack, piece, direction);
					fall_20g(stack, piece, height);
					while(move(stack, piece, direction))
						fall_20g(stack, piece, height);
				} else {
					fall_20g(stack, piece, height);
				}
				break;

			case 'k':
				rotate(stack, piece, CW);
				fall_20g(stack, piece, height);
				break;

			default:
				printf("Invalid input character detected.\n");
				break;
		}
		input_i += 1;
	}

	return game_over;
}
