#include <stdio.h>
#include <check.h>
#include "game.h"
#include "test_main.h"

START_TEST (I_0)
{
    int stack[30][10] = {0};
    memcpy(stack[19], ((int [10]){0, 0, 0, 1, 1, 1, 1, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {I, _0_, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[19], ((int [10]){0, 0, 0, 1, 1, 1, 1, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (I_CCW)
{
    int stack[30][10] = {0};
    memcpy(stack[20], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[19], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[17], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {I, CCW, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[20], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[19], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[17], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (I_180)
{
    int stack[30][10] = {0};
    memcpy(stack[19], ((int [10]){0, 0, 0, 1, 1, 1, 1, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {I, _180_, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[19], ((int [10]){0, 0, 0, 1, 1, 1, 1, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (I_CW)
{
    int stack[30][10] = {0};
    memcpy(stack[20], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[19], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[17], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {I, CW, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[20], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[19], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[17], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (Z_0)
{
    int stack[30][10] = {0};
    memcpy(stack[19], ((int [10]){0, 0, 0, 1, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {Z, _0_, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[19], ((int [10]){0, 0, 0, 1, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (Z_CCW)
{
    int stack[30][10] = {0};
    memcpy(stack[20], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[19], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {Z, CCW, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[20], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[19], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (Z_180)
{
    int stack[30][10] = {0};
    memcpy(stack[19], ((int [10]){0, 0, 0, 1, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {Z, _180_, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[19], ((int [10]){0, 0, 0, 1, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (Z_CW)
{
    int stack[30][10] = {0};
    memcpy(stack[20], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[19], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {Z, CW, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[20], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[19], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (S_0)
{
    int stack[30][10] = {0};
    memcpy(stack[19], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 1, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {S, _0_, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[19], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 1, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (S_CCW)
{
    int stack[30][10] = {0};
    memcpy(stack[20], ((int [10]){0, 0, 0, 1, 0, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[19], ((int [10]){0, 0, 0, 1, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {S, CCW, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[20], ((int [10]){0, 0, 0, 1, 0, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[19], ((int [10]){0, 0, 0, 1, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (S_180)
{
    int stack[30][10] = {0};
    memcpy(stack[19], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 1, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {S, _180_, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[19], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 1, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (S_CW)
{
    int stack[30][10] = {0};
    memcpy(stack[20], ((int [10]){0, 0, 0, 1, 0, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[19], ((int [10]){0, 0, 0, 1, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {S, CW, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[20], ((int [10]){0, 0, 0, 1, 0, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[19], ((int [10]){0, 0, 0, 1, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (J_0)
{
    int stack[30][10] = {0};
    memcpy(stack[19], ((int [10]){0, 0, 0, 1, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {J, _0_, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[19], ((int [10]){0, 0, 0, 1, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (J_CCW)
{
    int stack[30][10] = {0};
    memcpy(stack[20], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[19], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {J, CCW, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[20], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[19], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (J_180)
{
    int stack[30][10] = {0};
    memcpy(stack[19], ((int [10]){0, 0, 0, 1, 0, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 1, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {J, _180_, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[19], ((int [10]){0, 0, 0, 1, 0, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 1, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (J_CW)
{
    int stack[30][10] = {0};
    memcpy(stack[20], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[19], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 1, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {J, CW, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[20], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[19], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 1, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (L_0)
{
    int stack[30][10] = {0};
    memcpy(stack[19], ((int [10]){0, 0, 0, 1, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 1, 0, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {L, _0_, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[19], ((int [10]){0, 0, 0, 1, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 1, 0, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (L_CCW)
{
    int stack[30][10] = {0};
    memcpy(stack[20], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[19], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {L, CCW, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[20], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[19], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (L_180)
{
    int stack[30][10] = {0};
    memcpy(stack[19], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 1, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {L, _180_, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[19], ((int [10]){0, 0, 0, 0, 0, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 1, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (L_CW)
{
    int stack[30][10] = {0};
    memcpy(stack[20], ((int [10]){0, 0, 0, 1, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[19], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {L, CW, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[20], ((int [10]){0, 0, 0, 1, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[19], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (O_0)
{
    int stack[30][10] = {0};
    memcpy(stack[19], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {O, _0_, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[19], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (O_CCW)
{
    int stack[30][10] = {0};
    memcpy(stack[19], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {O, CCW, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[19], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (O_180)
{
    int stack[30][10] = {0};
    memcpy(stack[19], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {O, _180_, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[19], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (O_CW)
{
    int stack[30][10] = {0};
    memcpy(stack[19], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {O, CW, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[19], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (T_0)
{
    int stack[30][10] = {0};
    memcpy(stack[19], ((int [10]){0, 0, 0, 1, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {T, _0_, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[19], ((int [10]){0, 0, 0, 1, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (T_CCW)
{
    int stack[30][10] = {0};
    memcpy(stack[20], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[19], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {T, CCW, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[20], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[19], ((int [10]){0, 0, 0, 0, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (T_180)
{
    int stack[30][10] = {0};
    memcpy(stack[19], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 1, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {T, _180_, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[19], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 1, 1, 1, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

START_TEST (T_CW)
{
    int stack[30][10] = {0};
    memcpy(stack[20], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[19], ((int [10]){0, 0, 0, 1, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(stack[18], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    struct piece_state piece = {T, CW, 3, 20};
    int height = 0;
    char input[] = "s\n";

    int game_over = input_sequence(input, stack, &piece, &height);

    int answer[30][10] = {0};
    memcpy(answer[20], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[19], ((int [10]){0, 0, 0, 1, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    memcpy(answer[18], ((int [10]){0, 0, 0, 0, 1, 0, 0, 0, 0, 0}), sizeof(int) * 10);
    ck_assert(memcmp(&stack, &answer, sizeof(int)*30*10) == 0);
    ck_assert(game_over == 4);
}
END_TEST

Suite *make_deathspawn_suite (void)
{
    Suite *s;
    TCase *tc;
    s = suite_create("Piece Spawning");
    tc = tcase_create("Death");

    tcase_add_test(tc, I_0);
    tcase_add_test(tc, I_CCW);
    tcase_add_test(tc, I_180);
    tcase_add_test(tc, I_CW);

    tcase_add_test(tc, Z_0);
    tcase_add_test(tc, Z_CCW);
    tcase_add_test(tc, Z_180);
    tcase_add_test(tc, Z_CW);

    tcase_add_test(tc, S_0);
    tcase_add_test(tc, S_CCW);
    tcase_add_test(tc, S_180);
    tcase_add_test(tc, S_CW);

    tcase_add_test(tc, J_0);
    tcase_add_test(tc, J_CCW);
    tcase_add_test(tc, J_180);
    tcase_add_test(tc, J_CW);

    tcase_add_test(tc, L_0);
    tcase_add_test(tc, L_CCW);
    tcase_add_test(tc, L_180);
    tcase_add_test(tc, L_CW);

    tcase_add_test(tc, O_0);
    tcase_add_test(tc, O_CCW);
    tcase_add_test(tc, O_180);
    tcase_add_test(tc, O_CW);

    tcase_add_test(tc, T_0);
    tcase_add_test(tc, T_CCW);
    tcase_add_test(tc, T_180);
    tcase_add_test(tc, T_CW);
    
    suite_add_tcase(s, tc);

    return s;
}
