#ifndef TEST_MAIN_H
#define TEST_MAIN_H

void draw_test(int stack[30][10], int answer[30][10]);

Suite *make_freespawn_suite();
Suite *make_deathspawn_suite();

#endif /* TEST_MAIN_H */