#include <stdio.h>
#include <check.h>
#include "test_main.h"

int main (void) {
  int n_failed;
  SRunner *sr;

  sr = srunner_create (make_freespawn_suite());
  srunner_add_suite(sr, make_deathspawn_suite());

  srunner_run_all (sr, CK_NORMAL);
  n_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return n_failed;
}

void draw_test(int stack[30][10], int answer[30][10]) {

    for(int i = 0; i < 30; i++) {
        if (29 - i <= 19)
            printf("  ║");
        else
            printf("   ");
        for (int j = 0; j < 10; j++) {
            switch(stack[29 - i][j])
            {
                case 0:
                    printf("  ");
                    break;
                case 1:
                    printf("██");
                    break;
                default:
                    printf("..");
                    break;
            }
        }
        if (29 - i <= 19)
            printf("║");
        printf("\n");
    }
    printf("  ╚════════════════════╝\n");

   for(int i = 0; i < 30; i++) {
        if (29 - i <= 19)
            printf("  ║");
        else
            printf("   ");
        for (int j = 0; j < 10; j++) {
            switch(answer[29 - i][j])
            {
                case 0:
                    printf("  ");
                    break;
                case 1:
                    printf("██");
                    break;
                default:
                    printf("..");
                    break;
            }
        }
        if (29 - i <= 19)
            printf("║");
        printf("\n");
    }
    printf("  ╚════════════════════╝\n");


    return;
}